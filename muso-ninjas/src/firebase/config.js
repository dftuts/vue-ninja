import { initializeApp } from "firebase/app"
import { getFirestore, serverTimestamp } from "firebase/firestore"
import { getAuth } from "firebase/auth"
import { getStorage, ref, uploadBytes, getDownloadURL } from "firebase/storage"

const firebaseConfig = {
  apiKey: "AIzaSyBsCL_sSfApHJIqiAQwhQBgvME2EfvG5lM",
  authDomain: "muso-ninjas-f43d3.firebaseapp.com",
  projectId: "muso-ninjas-f43d3",
  storageBucket: "muso-ninjas-f43d3.appspot.com",
  messagingSenderId: "1068200125592",
  appId: "1:1068200125592:web:f101aff1f2d89e54fd1ced",
}

//Initialize Firebase
const app = initializeApp(firebaseConfig)

// init firestore service
const auth = getAuth(app)
const db = getFirestore(app)
const timestamp = serverTimestamp()
const storage = getStorage(app)
const sRef = ref

// export firestore
export { db, auth, timestamp, storage, sRef, uploadBytes, getDownloadURL }
