import { ref } from "@vue/reactivity"
import { watchEffect } from "vue"
import { db } from "../firebase/config"

import { doc, onSnapshot } from "firebase/firestore"

// retrieves a single document from firebase

const getDocument = (col, id) => {
  const document = ref(null)
  const error = ref(null)

  const docRef = doc(db, col, id)

  const unsub = onSnapshot(
    docRef,
    (doc) => {
      if (doc.data()) {
        document.value = { ...doc.data(), id: doc.id }
        error.value = null
      } else {
        error.value = "That document does not exist"
      }
    },
    (err) => {
      console.log(err.message)
      error.value = "Could not fetch document"
    }
  )

  watchEffect((onInvalidate) => {
    onInvalidate(() => unsub())
  })

  return { document, error }
}

export default getDocument
