// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getFirestore, serverTimestamp } from "firebase/firestore";
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCzMzofRjp2Vtf68mR2EwdkrUWmVDaFVog",
  authDomain: "udemy-vue-firebase-3ab98.firebaseapp.com",
  projectId: "udemy-vue-firebase-3ab98",
  storageBucket: "udemy-vue-firebase-3ab98.appspot.com",
  messagingSenderId: "160776174299",
  appId: "1:160776174299:web:dbf06173e3d154f26aeafe",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// init firestore service
const projectFirestore = getFirestore(app);
const timestamp = serverTimestamp();

// export firestore
export { projectFirestore, timestamp };
