const app = Vue.createApp({
  // data, functions
  //template: '<h2>I am the template</h2>'

  data(){
    return {
      title: 'The Final Empire',
      author: 'Brandon Sanderson',
      age: 45,
      showBooks: true,
      x:0,
      y:0
    }
  },
  methods: {
    changeAge( rule ){
      rule === 'plus' ? this.age++ : this.age--
    },
    changeTitle(title){
      this.title = title
    },
    toggleShowBooks(){
      this.showBooks = !this.showBooks
    },
    handleEvent( event, data ) {
      console.log(event.type, data);
    },
    handleMousemove(e){
      this.x = e.offsetX
      this.y = e.offsetY
      
    }
  }

}).mount('#app');