const app = Vue.createApp({
  data(){
    return {
      url:'http://www.google.com',
      showBooks:true,
      // title: 'Eye of The World',
      // author: 'Robert Jordan',
      // age: 'Deceased',
      books:[
        { title: 'Name of the Wind', author: 'Patrick Rothfuss', img: 'img/botw.jpg', isFav:true},
        { title: 'Eye of the World', author: 'Robert Jordan', img: 'img/botw.jpg', isFav:false},
        { title: 'Gathering Storm', author:'Brandon Sanderson', img: 'img/botw.jpg', isFav:true}
      ],
      x:0,
      y:0
    }
  }, 
  methods: {
    toggleBooks(){
      this.showBooks = !this.showBooks
    },
    handleEvent(e, data){
      console.log(e.type)
    },
    handleMouseMove(e){
      this.x = e.offsetX
      this.y = e.offsetY
    },
    toggleFav( book ){
      book.isFav = !book.isFav
    }
  },
  computed: {
    filteredBooks() {
      return this.books.filter( book => book.isFav )
    }
  }
}).mount('#app');