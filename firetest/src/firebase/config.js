import { initializeApp } from "firebase/app"
import { getFirestore } from "firebase/firestore"

const firebaseConfig = {
  apiKey: "AIzaSyCfYR1cvBm1qeAkAao1xNmtjVaXpRi5OqY",
  authDomain: "firetest-28fca.firebaseapp.com",
  projectId: "firetest-28fca",
  storageBucket: "firetest-28fca.appspot.com",
  messagingSenderId: "1090948994509",
  appId: "1:1090948994509:web:bdfee84dc0786e2077e084",
}

initializeApp(firebaseConfig)

const db = getFirestore()

export default { db }
