import { initializeApp } from "firebase/app"
import { getFirestore, serverTimestamp } from "firebase/firestore"
import { getAuth } from "firebase/auth"

const firebaseConfig = {
  apiKey: "AIzaSyCzMzofRjp2Vtf68mR2EwdkrUWmVDaFVog",
  authDomain: "udemy-vue-firebase-3ab98.firebaseapp.com",
  projectId: "udemy-vue-firebase-3ab98",
  storageBucket: "udemy-vue-firebase-3ab98.appspot.com",
  messagingSenderId: "160776174299",
  appId: "1:160776174299:web:19853e9c6c6169616aeafe",
}

//Initialize Firebase
const app = initializeApp(firebaseConfig)

// init firestore service
const auth = getAuth(app)
const db = getFirestore(app)
const timestamp = serverTimestamp()

// export firestore
export { db, auth, timestamp }
