import { signInWithEmailAndPassword } from "firebase/auth"
import { ref } from "vue"
import { auth } from "../firebase/config"

const error = ref(null)

const login = async (email, password) => {
  error.value = null

  try {
    const res = await signInWithEmailAndPassword(auth, email, password)
    error.value = null
    console.log(res)
    return res
  } catch (err) {
    console.log(err)
    error.value = "Incorrect Login Credentials"
  }
}

const useLogin = () => {
  return { error, login }
}

export default useLogin
