import { ref } from "@vue/reactivity"
import { db } from "../firebase/config"
import { collection, onSnapshot, query, orderBy } from "@firebase/firestore"
import { watchEffect } from "vue"

const getCollection = (col) => {
  const documents = ref(null)
  const error = ref(null)

  const collectionRef = query(collection(db, col), orderBy("createdAt", "asc"))

  const unsub = onSnapshot(
    collectionRef,
    (snapshot) => {
      console.log(snapshot)
      let results = []
      snapshot.docs.forEach((doc) => {
        doc.data().createdAt && results.push({ ...doc.data(), id: doc.id })
      })
      documents.value = results
      error.value = null
    },
    (err) => {
      console.log(err.message)
      documents.value = null
      error.value = "could not fetch data"
    }
  )

  watchEffect((onInvalidate) => {
    onInvalidate(() => unsub())
  })

  return { documents, error }
}

export default getCollection
