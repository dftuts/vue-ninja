import { ref } from "vue"
import { db } from "../firebase/config"
import { addDoc, collection } from "firebase/firestore"

const useCollection = (col) => {
  const error = ref(null)

  const addMessage = async (doc) => {
    error.value = null

    try {
      await addDoc(collection(db, col), doc)
    } catch (err) {
      console.log(err.message)
      error.value = "Could not send the message"
    }
  }

  return { error, addMessage }
}

export default useCollection
