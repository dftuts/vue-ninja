import { createUserWithEmailAndPassword, updateProfile } from "firebase/auth"
import { ref } from "vue"
import { auth } from "../firebase/config"

const error = ref(null)

const signup = async (email, password, displayName) => {
  error.value = null

  try {
    const res = await createUserWithEmailAndPassword(auth, email, password)

    // await sendEmailVerification(res.user).catch((err) =>
    //   console.log(err)
    // )

    if (!res) {
      throw new Error("Could not complete signup")
    }

    await updateProfile(res.user, { displayName: displayName })
    return res
  } catch (err) {
    console.log(err)
    error.value = err.message
  }
}

const useSignup = () => {
  return { error, signup }
}

export default useSignup
