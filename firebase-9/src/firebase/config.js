import { initializeApp } from "firebase/app"
import { getFirestore } from "firebase/firestore"
import { getAuth } from "firebase/auth"

const firebaseConfig = {
  apiKey: "AIzaSyDsUL-qPdPGrP7w1GRvO_Jpla2qegRhn5g",
  authDomain: "fir-9-3c360.firebaseapp.com",
  projectId: "fir-9-3c360",
  storageBucket: "fir-9-3c360.appspot.com",
  messagingSenderId: "257868793821",
  appId: "1:257868793821:web:236f565da8ae30f57dd834",
}

//init firebase
initializeApp(firebaseConfig)

// init services
const db = getFirestore()
const auth = getAuth()

export { db, auth }
